﻿using System.Threading.Tasks;
using Dapper;
using Discount.API.Entities;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Discount.API.Repositories
{
    public class DiscountRepository : IDiscountRepository
    {
        private readonly IConfiguration _configuration;

        public DiscountRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<Coupon> GetDiscount(string productName)
        {
            using var connection = new NpgsqlConnection(
               _configuration.GetValue<string>("DatabaseSettings:ConnectionString"));

            var coupon = await connection.QueryFirstOrDefaultAsync<Coupon>
                ("SELECT * FROM coupon WHERE productname=@ProductName", new
                {
                    ProductName = productName
                });

            if (coupon == null)
                return new Coupon
                { Amount = 0, ProductName = "No Discount", Description = "No Discount Desc" };


            return coupon;
        }

        public async Task<bool> CreateDiscount(Coupon coupon)
        {
            using var connection = new NpgsqlConnection(
                _configuration.GetValue<string>("DatabaseSettings:ConnectionString"));

            var affected =
                await connection.ExecuteAsync(
                    "INSERT INTO COUPON (ProductName,Description,Amount) VALUES (@ProductName,@Description,@Amount)",
                    new
                    {
                        coupon.ProductName,
                        coupon.Description,
                        coupon.Amount
                    });

            return affected > 0;
        }

        public async Task<bool> UpdateDiscount(Coupon coupon)
        {
            using var connection = new NpgsqlConnection(
                _configuration.GetValue<string>("DatabaseSettings:ConnectionString"));

            var affected =
                await connection.ExecuteAsync(
                    "UPDATE COUPON SET ProductName=@ProductName," +
                    "Description=@Description,Amount=@Amount where Id=@Id",
                    new
                    {
                        coupon.ProductName,
                        coupon.Description,
                        coupon.Amount,
                        coupon.Id
                    });

            return affected > 0;
        }

        public async Task<bool> DeleteDiscount(string productName)
        {
            using var connection = new NpgsqlConnection(
                _configuration.GetValue<string>("DatabaseSettings:ConnectionString"));

            var affected =
                await connection.ExecuteAsync(
                    "DELETE FROM COUPON where productName=@productName",
                    new
                    {
                        productName
                    });

            return affected > 0;
        }
    }
}